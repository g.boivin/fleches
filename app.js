// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBKthDqj65tEeq9CilzQa5GHDlHhRBSHNg",
  authDomain: "fleches-341bf.firebaseapp.com",
  databaseURL: "https://fleches-341bf.firebaseio.com",
  projectId: "fleches-341bf",
  storageBucket: "fleches-341bf.appspot.com",
  messagingSenderId: "110439560149",
  appId: "1:110439560149:web:5919deddbf6f6530f0035c",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let width = 13;
let height = 18;
const body = document.getElementById("grid-view");
let colors = [
  "1,87,155",
  "136,14,79",
  "183,28,28",
  "27,94,32",
  "249,168,37",
  "245,127,23",
  "29,233,182",
];
let players = [];
const types = {
  DEFINITION: "definition",
  LETTER: "letter",
};
let gameId = document.location.search
  ? document.location.search.split("=")[1]
  : null;

let playerId = gameId ? localStorage.getItem(gameId) : null;

let gamePath = `/games/${gameId}`;
let gridPath = `${gamePath}/grid`;
let playersPath = `${gamePath}/players/`;
let prevDirection;

let tempDirection = 'horizontal';
let currentlySelected;
const updates = [];

const directions = {
  DR: "down-right",
  D: "down",
  R: "right",
  RD: "right-down",
};
const initGrid = (width, height) => {
  const grid = [];
  for (let i = 0; i < height; i++) {
    grid[i] = [];
  }
  return grid;
};
let grid = initGrid(width, height);

const cursorDirection = directions.D;

const getLeft = (row, col) => getCellInput(row, col - 1);

const getRight = (row, col) => getCellInput(row, col + 1);

const getTop = (row, col) => getCellInput(row - 1, col);

const getBottom = (row, col) => getCellInput(row + 1, col);

const getBestDirection = (row, col) => {
  let direction;
  let top = getTop(row, col);
  let left = getLeft(row, col);
  let right = getRight(row, col);
  let down = getBottom(row, col);

  console.log(tempDirection)
  if (tempDirection) {
    prevDirection = tempDirection;
    return tempDirection;
  }

  //Am I first cell?
  if (row === 0) {
    direction = "vertical";
  } else if (col === 0) {
    direction = "horizontal";
  }
  //Only one direction possible?
  else if (!right) {
    direction = "vertical";
  } else if (!down) {
    direction = "horizontal";
  } else if (right && right.value) {
    direction = "vertical";
  } else if (down && down.value) {
    direction = "horizontal";
  } else if (left && left.value && (!top || (top && !top.value))) {
    direction = "horizontal";
    //letter up filled and letter left not filled
  } else if (top && top.value && (!left || (left && !left.value))) {
    direction = "vertical";
    //empty top definition left;
  } else if (top && !top.value && !left) {
    direction = "horizontal";
  } else if (left && !left.value && !top) {
    direction = "vertical";
  } else {
    direction = "horizontal";
  }

  if (!direction) direction = prevDirection;

  //letter up filled and letter left not filled

  prevDirection = direction;
  return direction;
  // first cell for one word?
  //word direction
};

const extractGridFromML = (json) => {
  let rows = 18;
  let cols = 13;
  const grid = [];
  const vertices = json.textAnnotations[0].boundingPoly.vertices;
  const height = vertices[vertices.length -1].y - vertices[0].y;
  const width = vertices[1].x - vertices[0].x;

  const avgBlockWidth = width/cols;
  const avgBlockHeight = height/rows;

  json.fullTextAnnotation.pages.forEach((p, i) => {
    p.blocks.forEach((block, j) => {
      //Find out row for current block
      let row = Math.floor(block.boundingBox.vertices[1].y / avgBlockHeight);
      let col = Math.floor(block.boundingBox.vertices[2].x / avgBlockWidth);
      console.log(`${row}_${col}`)
      if (!grid[row]) {
        grid[row] = [];
      }
      if (!grid[row][col]) {
        grid[row][col] = [];
      }
      block.paragraphs.forEach(paragraph => {
        let definition = '';
        paragraph.words.forEach( word => {
          definition += definition ? ' ' : '';

            word.symbols.forEach(s => {
              definition += s.text;
            });
          });
          console.log(definition)
          let regexp = /[a-zA-Z]/g;
          if (regexp.test(definition)) {
            let direction;
            if (row === 0 || col === 0) {
              if (row === 0) {
                if (grid[row][col].definitions) {
                  direction = 'down';
                } else {
                  direction = 'right-down';
                }
                if (col === 0 && row === 0) {
                  if (grid[row][col].definitions) {
                    direction = 'down-right';
                  } else {
                    direction = 'right-down';
                  }
                }
              } else if (col === 0) {
                if (grid[row][col].definitions) {
                  direction = 'down-right';
                } else {
                  direction = 'right';
                }
              }
              if (col === cols - 1) {
                direction = 'down';
              }
            } else if (col === cols - 1) {
              direction = 'down';
            } else if (row === rows - 1) {
              direction = 'right';
            } else {
              if (grid[row][col].definitions) {
                direction = 'down';
              } else {
                direction = 'right';
              }
            }



            grid[row][col] = {
              type: 'definition',
              definitions: [
                ...grid[row][col].definitions || [],
                {
                  text: definition,
                  direction: direction,
                }
              ]
          }
        }
      });
      

    });
  });
  return grid;
}


const getCellInput = (row, col) => document.getElementById(`${row}/${col}`);

const getBestNextCell = (row, col) => {
  const bestDirection = getBestDirection(row, col);
  let bestNextCell;
  switch (bestDirection) {
    case "vertical":
      bestNextCell = [row + 1, col];
      break;
    case "horizontal":
      bestNextCell = [row, col + 1];
      break;
  }

  if (!getCellInput(...bestNextCell)) return [row, col];
  return bestNextCell;
};

const getBestPrevCell = (row, col) => {
  const bestDirection = getBestDirection(row, col);
  let bestPrevCell;
  switch (bestDirection) {
    case 'vertical':
      bestPrevCell = [row - 1, col];
      break;
    case 'horizontal':
      bestPrevCell = [row, col - 1];
      break;
  }

  if (!getCellInput(...bestPrevCell)) return [row, col];
  return bestPrevCell;
};

const showPlayerPrompt = () => {
  const overlay = document.getElementsByClassName("overlay")[0];
  overlay.classList.add("show");
  const playerInput = document.getElementsByName("playerName")[0];
  playerInput.addEventListener("keydown", addPlayer);
};

const addPlayer = (e) => {
  const overlay = document.getElementsByClassName("overlay")[0];
  if (e.key === "Enter") {
    const player = {
      name: e.target.value,
      color: randomElement(colors),
    };
    const ref = firebase.database().ref(playersPath).push(player);
    playerId = ref.path.pieces_[ref.path.pieces_.length - 1];
    localStorage.setItem(gameId, playerId);
    overlay.classList.remove("show");
  }
};

const loadGrid = () => {
  firebase
    .database()
    .ref(gridPath)
    .once("value")
    .then(function (snapshot) {
      grid = snapshot.val();
      //grid = fillData(grid);
      drawGrid(grid);
    });
};

const loadPlayers = () => {
  firebase
    .database()
    .ref(playersPath)
    .on("value", function (p) {
      players = p.val();
      renderPlayers(players);
    });
};

if (gameId) {
  document.getElementById("players").style.display = "block";
  loadPlayers();
  if (!playerId) showPlayerPrompt();
} else {
  const createDom = document.getElementById("create");
  createDom.style.display = "block";
}
//const grid = initGrid(width, height);

const drawGrid = (grid) => {
  body.innerHTML = "";
  for (let i = 0; i < height; i++) {
    grid[i] = grid[i] || [];
    let row = document.createElement("div");
    row.className = "row";
    body.append(row);
    for (let j = 0; j < width; j++) {
      let cell = document.createElement("div");
      cell.className = "cell";
      cell.append(renderCell(grid[i][j], i, j));
      row.append(cell);
    }
  }
};
document.getElementsByName("cols")[0].addEventListener("change", (e) => {
  width = e.target.value;
  drawGrid(grid);
});
document.getElementsByName("rows")[0].addEventListener("change", (e) => {
  height = e.target.value;
  drawGrid(grid);
});

const renderCreateDefinition = (dom, x, y ) => {
  dom.className = "create";
  const button = document.createElement("button");
      button.innerText = "+";
      button.id = `${x}/${y}`;
      button.addEventListener("click", (e) => {
        const selected = document.querySelector(".selected");
        if (selected) selected.classList.remove("selected");
        let firstDef = grid[x][y] && grid[x][y].definitions && grid[x][y].definitions[0] ? grid[x][y].definitions[0] : null;
        let secondDef = grid[x][y] && grid[x][y].definitions && grid[x][y].definitions[1] ? grid[x][y].definitions[1] : null;

        let firstDefText = firstDef ? firstDef.text : '';
        let secondDefText = secondDef ? secondDef.text : '';
        let firstDefDirection = firstDef ? firstDef.direction : '';
        let secondDefDirection = secondDef ? secondDef.direction : '';

        document.getElementsByName("definition")[0].value = firstDefText;
        document.getElementsByName("definition")[0].focus();
        document.getElementsByName("definition2")[0].value = secondDefText;
        document.getElementsByName("direction")[0].value = firstDefDirection;
        document.getElementsByName("direction2")[0].value = secondDefDirection;

        document.getElementById("cellEdit").dataset.coords = e.target.id;
        document.getElementById("cellEdit").style.display = "block";
        e.target.parentNode.classList.add("selected");
      });
      dom.appendChild(button);
}
const renderLetter = (dom, cell, x, y) => {
  dom.classList.add("letter");
  if (cell && cell.line) {
    dom.dataset.line = cell.line;
  }
  const input = document.createElement("input");
  input.type = "text";
  input.autocomplete = false;
  input.id = `${x}/${y}`;
  input.value = cell ? cell.letter : "";
  if (gameId) {
  const color = getPlayerColor(cell ? cell.user : null);
  if(color) input.style.backgroundColor = color;
  input.addEventListener("keyup", updateLetter);
  input.addEventListener("keydown", silentKiller);

  var starCountRef = firebase.database().ref(gridPath + "/" + input.id);
  starCountRef.on("value", function (snapshot) {
    const updatedInput = document.getElementById(input.id);
    const val = snapshot.val();
    updatedInput.value = val ? val.letter : "";
    updatedInput.style.backgroundColor = getPlayerColor(
      val ? val.user : null
    );
    grid[x][y] = { type: types.LETTER, ...val};
  });
}
  dom.appendChild(input);
}
const renderDefinitions = (dom, cell) => {
  dom.classList.add(cell.type);
  cell.definitions.forEach((element) => {
    let definition = document.createElement("div");
    definition.innerText = element.text;
    definition.dataset.direction = element.direction;
    dom.appendChild(definition);
  });
}

function renderCell(cell, x, y) {
  const cellDOM = document.createElement("div");

  if (!gameId) {
    cellDOM.classList.add("create");
    renderCreateDefinition(cellDOM, x, y);
  }

  if ((gameId && (!cell || !cell.definitions)) || (cell && cell.line)) {
    renderLetter(cellDOM, cell, x, y);
  }

  if (cell && cell.definitions) {
    renderDefinitions(cellDOM, cell)
  }

  return cellDOM;
}

const getPlayerColor = (id) => {
  return players && players[id] ? `rgba(${players[id].color}, 0.1)` : "";
};

const silentKiller = (e) => {
  if (
    !e.metaKey &&
    (String.fromCharCode(e.keyCode) === e.target.value ||
      (e.keyCode >= 65 && e.keyCode <= 90))
  ) {
    e.preventDefault();
  }
};
const updateLetter = (e) => {
  const coords = e.target.id.split("/").map((n) => parseInt(n));
  if (e.keyCode >= 65 && e.keyCode <= 90 && !e.metaKey) {
    e.preventDefault();
    e.target.value = String.fromCharCode(e.keyCode);
    moveCursor(
      ...getBestNextCell(...coords)
    );
  }
  else if (e.keyCode == 8) {
    let newPosition;
    if (updates.length) {
      const lastPosition = updates.pop();
      const sameRow = lastPosition[0] < coords[0] && lastPosition[1] === coords[1];
      const sameCol = lastPosition[1] < coords[1] && lastPosition[0] === coords[0];
      if (sameCol || sameRow) {
        newPosition = lastPosition
      }
    }
    if (tempDirection) {
      newPosition = newPosition ? newPosition : getBestPrevCell(...coords);
      moveCursor(...newPosition);
    }
  }
  const update = { letter: e.target.value };
  if (e.target.dataset.line) {
    update.line = e.target.dataset.line;
  }
  if (e.target.value) {
    let cell =  grid[coords[0]][coords[1]];
    if (cell && (e.target.value === cell.letter)) {
      console.log(cell);
      update.user = cell.user;
    } else {
      update.user = playerId;
      updates.push(coords);
    }
  } else {
    delete update.user;
  }
  grid[coords[0]][coords[1]] = { type: types.LETTER, letter: update.letter};
  firebase.database().ref(`${gridPath}/${e.target.id}`).set(update);
};

const renderPlayers = (players) => {
  const dom = document.getElementById("players");
  dom.innerHTML = "";
  console.log(players);
  if (players) Object.values(players).forEach((p) => {
    const player = document.createElement("div");
    player.style.backgroundColor = `rgba(${p.color}, .1)`;
    const color = document.createElement("span");
    color.className = "color";
    color.style.background = `rgb(${p.color})`;
    colors = colors.filter((c) => c !== p.color);
    player.innerText = p.name;
    player.appendChild(color);
    dom.appendChild(player);
  });
};

const randomElement = (list) => list[Math.floor(Math.random() * list.length)];

if (window.location.search) {
  loadGrid();
} else {
  drawGrid(grid);
}

const submitCellEdit = () => {
  const definition = document.getElementsByName("definition")[0].value;
  const direction = document.getElementsByName("direction")[0].value;

  const definition2 = document.getElementsByName("definition2")[0].value;
  const direction2 = document.getElementsByName("direction2")[0].value;

  const definitions = [];
  if (definition) {
    definitions.push({
      text: definition,
      direction,
    });
  }

  if (definition2) {
    definitions.push({
      text: definition2,
      direction: direction2,
    });
  }

  const coords = document.getElementById("cellEdit").dataset.coords.split("/");
  if (definitions.length === 0) {
    delete grid[coords[0]][coords[1]];
  } else {
    grid[coords[0]][coords[1]] = {
      type: types.DEFINITION,
      definitions,
    };
  }
  drawGrid(grid);
};

const toggleLineDown = (e) => {
  const coords = document.getElementById("cellEdit").dataset.coords.split("/");
  if (!e.target.checked) {
    delete(grid[coords[0]][coords[1]]);
  } else {
    grid[coords[0]][coords[1]] = {
      letter:"",
      line: directions.D
    }
  }
  drawGrid(grid);
}
const toggleLineRight = (e) => {
  const coords = document.getElementById("cellEdit").dataset.coords.split("/");
  if (!e.target.checked) {
    delete(grid[coords[0]][coords[1]]);
  } else {
    grid[coords[0]][coords[1]] = {
      letter:"",
      line: directions.R
    }
  }
  drawGrid(grid);
}

document
  .getElementsByName("lineRight")[0]
  .addEventListener("change", toggleLineRight);

  document
  .getElementsByName("lineDown")[0]
  .addEventListener("change", toggleLineDown);

document
  .getElementsByName("definition")[0]
  .addEventListener("keyup", submitCellEdit);
document
  .getElementsByName("definition2")[0]
  .addEventListener("keyup", submitCellEdit);
document
  .getElementsByName("direction")[0]
  .addEventListener("change", submitCellEdit);
document
  .getElementsByName("direction2")[0]
  .addEventListener("change", submitCellEdit);

  const loadML = (e) => {
    grid = extractGridFromML(JSON.parse(e.target.value));
    console.log(grid);
    drawGrid(grid);
  }

  document
  .getElementsByName("ml")[0]
  .addEventListener("change", loadML);



const createNew = () => {
  const ref = firebase
    .database()
    .ref("/games/")
    .push({
      grid: grid,
      players: [],
    })
    .then((p) => {
      const id = p.path.pieces_[p.path.pieces_.length - 1];
      window.location.href = `/?id=${id}`;
    });
};

document
  .getElementsByName("createButton")[0]
  .addEventListener("click", createNew);

const onBlur = (e) => {
  //e.target.parentNode.classList.remove("vertical");
  //e.target.parentNode.classList.remove("horizontal");
};

const selectCell = (e, autoMode) => {
  if ((e.target.type = "text")) {
    [...document.querySelectorAll('.horizontal,.vertical')].forEach(el => {
      if (e.target.parentNode !== el) {
        el.classList.remove('horizontal');

        el.classList.remove('vertical'); 
      }
    })
    const targetInput = e.target;
    const { parentNode: parent } = targetInput;

    targetInput.addEventListener("blur", onBlur);
      if (
        parent.classList.contains("vertical")
      ) {
        parent.classList.remove("vertical");
        parent.classList.add("horizontal");
        tempDirection = 'horizontal';
      } else if (
        parent.classList.contains("horizontal")
      ) {
        parent.classList.remove("horizontal");
        parent.classList.add("vertical");
        tempDirection = 'vertical';
      } else {
        const direction = getBestDirection(
          ...targetInput.id.split("/").map((n) => parseInt(n))
        );
        if (!tempDirection) {
          parent.classList.add(direction);
    
          tempDirection = direction;
        }
        parent.classList.add(tempDirection);
      }
  }
};

const moveCursor = (row, col) => {
  document.getElementById(`${row}/${col}`).focus();
  //selectCell({ target: document.getElementById(`${row}/${col}`) }, true);
};
body.addEventListener("click", selectCell);

const getCellById = (coords) => {
  document.getElementById(`${coords[0]}/${coords[1]}`);
}

const getCurrentWords = (letter) => {
    const coords = letter.split("/").map((n) => parseInt(n));

    let currentWords = {
      vertical: {
        cells: [letter],
      },
      horizontal: {
        cells: [letter],
      }
    };
    let currentCoords = coords;
    console.log(currentCoords)
    while((currentCoords = [currentCoords[0], currentCoords[1] + 1]) && grid[currentCoords[0]] && grid[currentCoords[0]][currentCoords[1]] && grid[currentCoords[0]][currentCoords[1]].type !== types.DEFINITION) {
      console.log(currentCoords)
      currentWords.horizontal.cells.push(currentCoords.join('/'))
    }
    currentCoords = coords;
    while((currentCoords = [currentCoords[0], currentCoords[1] - 1]) &&  grid[currentCoords[0]]&& grid[currentCoords[0]][currentCoords[1]] && grid[currentCoords[0]][currentCoords[1]].type !== types.DEFINITION) {
      currentWords.horizontal.cells.push(currentCoords.join('/'))
    }
    currentWords.horizontal.definition = currentCoords;

    currentCoords = coords;

    while((currentCoords = [currentCoords[0] -1, currentCoords[1]]) &&  grid[currentCoords[0]] && grid[currentCoords[0]][currentCoords[1]] && grid[currentCoords[0]][currentCoords[1]].type !== types.DEFINITION) {
      console.log(currentCoords)
      currentWords.vertical.cells.push(currentCoords.join('/'))
    }
    currentWords.vertical.definition = currentCoords;
    currentCoords = coords;
    while((currentCoords = [currentCoords[0] +1, currentCoords[1]]) &&  grid[currentCoords[0]] && grid[currentCoords[0]][currentCoords[1]] && grid[currentCoords[0]][currentCoords[1]].type !== types.DEFINITION) {
      currentWords.vertical.cells.push(currentCoords.join('/'))
    }

  return currentWords;
}

const highlightWord = ({cells, definition}) => {
  [...document.getElementsByClassName('word')].forEach(e => e.classList.remove('word'));

  cells.forEach(l => {
    document.getElementById(l).parentNode.parentNode.classList.add('word')
  })
}
const highlightCurrentWord = (e) => {
  if (e.target.type === "text") {
    const currentWords = getCurrentWords(e.target.id);
    console.log(currentWords)
    highlightWord(currentWords[tempDirection])
  }
}


body.addEventListener("click", highlightCurrentWord);

